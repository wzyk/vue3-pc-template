import axios from 'axios'
import { signfn } from '@/utils/index'
import useStorage from '@/composables/useStorage'
const { get } = useStorage()
const http = axios.create({
  baseURL: import.meta.env.VITE_HTTP_URL,
  timeout: 10 * 1000,
  headers: { 'Content-Type': 'application/json;charset=UTF-8' }
})
http.interceptors.request.use(
  (config) => {
    const signInfo = signfn(config.url)
    config.headers = {
      ...config.headers,
      ...signInfo
    }
    if (get('token')) config.headers['auth-access-token'] = get('token')
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)
http.interceptors.response.use(
  (res) => {
    return res.data?.result
  },
  (error) => {
    return Promise.reject(error)
  }
)
export default http
