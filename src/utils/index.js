import CryptoJS from 'crypto-js'

function padZero(num, targetLength = 2) {
  let str = `${num}`
  while (str.length < targetLength) {
    str = `0${str}`
  }
  return str
}
const SECOND = 1000
const MINUTE = 60 * SECOND
const HOUR = 60 * MINUTE
const DAY = 24 * HOUR
export function parseTimeData(time) {
  const days = Math.floor(time / DAY)
  const hours = Math.floor((time % DAY) / HOUR)
  const minutes = Math.floor((time % HOUR) / MINUTE)
  const seconds = Math.floor((time % MINUTE) / SECOND)
  const milliseconds = Math.floor(time % SECOND)
  return {
    days,
    hours,
    minutes,
    seconds,
    milliseconds
  }
}
export function parseFormat(format, timeData) {
  let { days, hours, minutes, seconds, milliseconds } = timeData
  // 如果格式化字符串中不存在DD(天)，则将天的时间转为小时中去
  if (format.indexOf('DD') === -1) {
    hours += days * 24
  } else {
    // 对天补0
    format = format.replace('DD', padZero(days))
  }
  // 其他同理于DD的格式化处理方式
  if (format.indexOf('HH') === -1) {
    minutes += hours * 60
  } else {
    format = format.replace('HH', padZero(hours))
  }
  if (format.indexOf('mm') === -1) {
    seconds += minutes * 60
  } else {
    format = format.replace('mm', padZero(minutes))
  }
  if (format.indexOf('ss') === -1) {
    milliseconds += seconds * 1000
  } else {
    format = format.replace('ss', padZero(seconds))
  }
  return format.replace('SSS', padZero(milliseconds, 3))
}

export function replaceSearchKeyword(result, keyword) {
  const Reg = new RegExp(keyword, 'ig')
  let res = ''
  if (result) {
    res = result.replace(Reg, `<span style="color: #507DAF;">$&</span>`)
    return res
  }
  return result
}

export function copyToClip(text) {
  return new Promise((resolve, reject) => {
    try {
      const input = document.createElement('textarea')
      input.value = text
      input.setAttribute('readonly', 'readonly')
      document.body.appendChild(input)
      input.select()
      if (document.execCommand('copy')) {
        document.execCommand('copy')
        document.body.removeChild(input)
        resolve(text)
      }
    } catch (error) {
      reject(error)
    }
  })
}
function randomString() {
  let result = ''
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  const charactersLength = characters.length
  for (let i = 0; i < 32; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}

export function signfn(url, authKey = 'iosb83d14cd79b4098f3da9b4933ccdd', identify_key = 'ios11ee86c3fdc3d14cd9a48f3dgjkad') {
  const appKey = url === '/common/orginfo/get' ? 'iosb83d14cd79b4098f3da9b4933ccdd' : authKey
  const key = url === '/common/orginfo/get' ? 'ios11ee86c3fdc3d14cd9a48f3dgjkad' : identify_key
  const authTs = new Date().getTime()
  const authOnce = randomString()
  const authSign = CryptoJS.MD5(`act=${url}&app_key=${appKey}&nonce=${authOnce}&ts=${authTs}&key=${key}`)
      .toString()
  return {
    'auth-ts': authTs,
    'auth-nonce': authOnce,
    'auth-sign': authSign,
    'auth-key': appKey,
    'auth-type-id': 1
  }
}
