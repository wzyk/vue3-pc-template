import {h, render} from 'vue'
import MToast from './index.vue'

export default {
  install(app) {
    let vnode = null
    app.config.globalProperties.$showToast = (text) => {
      if (vnode) return
      vnode = h(MToast, {
        text,
        onHide: () => {
          hideToast()
        }
      })
      render(vnode, document.body)
      const vm = vnode.component
      vm.exposed.show.value = true

    }
    app.config.globalProperties.$hideToast = hideToast

    function hideToast() {
      if (vnode) {
        const vm = vnode.component
        vm.exposed.show.value = false
        vnode = null
      }
    }

  }
}
