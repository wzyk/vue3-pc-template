import { createApp } from 'vue'
import './assets/main.css'
import App from './App.vue'
import setupPlugins from './plugins'
import SvgIcon from '@/components/SvgIcon/index'
import 'virtual:svg-icons-register'
import MToast from '@/components/MToast/index.js'
function bootstrap() {
  const app = createApp(App)
  app.use(SvgIcon)
  app.use(MToast)
  setupPlugins(app)
  app.mount('#app')
}

bootstrap()
