import setupPinia from './pinia'
import setupRouter from './router'
import setupElementPlus from './elementPlus'
export default function setupPlugins(app) {
  setupRouter(app)
  setupPinia(app)
  setupElementPlus(app)

}
