import http from '@/utils/http'
export function configInfo() {
  return http.request({
    url: '/common/config/info/get',
    method: 'get'
  })
}
