import { defineStore } from 'pinia'
import { ref } from 'vue'

export default defineStore('configStore', () => {
  const count = ref(0)
  function setCount(data) {
    count.value = data
  }

  return {
    count,
    setCount
  }
})
